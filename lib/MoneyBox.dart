import 'package:flutter/material.dart';

class MoneyBox extends StatelessWidget {
  final String title; //ชื่อหมวดหมู่, รายการ
  final String currency; //สกุลเงิน
  final double amount; //จำนวนเงิน
  final Color color; //สีกล่อง
  final double size; //ขนาดกล่อง

  const MoneyBox({
    Key? key,
    required this.title,
    required this.currency,
    required this.amount,
    required this.color,
    required this.size,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(15.0),
      margin: const EdgeInsets.only(bottom: 16.0),
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(15),
      ),
      height: size,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: const TextStyle(
              fontSize: 20,
              color: Colors.black,
            ),
          ),
          Text(
            amount.toStringAsFixed(2),
            style: const TextStyle(
              fontSize: 20,
              color: Colors.black,
            ),
            textAlign: TextAlign.center,
          ),
          Text(
            currency,
            style: const TextStyle(
              fontSize: 20,
              color: Colors.black,
            ),
            textAlign: TextAlign.end,
          ),
        ],
      ),
    );
  }
}
