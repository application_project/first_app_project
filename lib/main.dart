import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'ExchangeRate.dart';
import 'MoneyBox.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My App',
      theme: ThemeData(
        useMaterial3: true,
        colorScheme: ColorScheme.fromSeed(
          seedColor: Colors.purple,
          brightness: Brightness.dark,
        ).copyWith(
          surface: Colors.orange[100],
          primary: Colors.orange,
        ),
        scaffoldBackgroundColor: Colors.orange[100],
        appBarTheme: const AppBarTheme(
          backgroundColor: Colors.orange,
        ),
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late Future<ExchangeRate> _futureExchangeRate;

  @override
  void initState() {
    super.initState();
    _futureExchangeRate = getExchangeRate();
  }

  Future<ExchangeRate> getExchangeRate() async {
    var url = Uri.parse("https://api.exchangerate-api.com/v4/latest/USD");
    var response = await http.get(url);

    if (response.statusCode == 200) {
      return exchangeRateFromJson(response.body);
    } else {
      throw Exception('Failed to load exchange rate');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Exchange Rates',
          style: TextStyle(
            fontSize: 24,
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: FutureBuilder<ExchangeRate>(
        future: _futureExchangeRate,
        builder: (BuildContext context, AsyncSnapshot<ExchangeRate> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasData) {
              var result = snapshot.data!;
              return ListView(
                children: [
                  ListTile(
                    title: Text(
                      'Date: ${result.date?.toLocal().toString().split(' ')[0]}',
                      style: const TextStyle(fontSize: 20, color: Colors.black54),
                    ),
                  ),
                  ...result.rates!.entries.map((entry) {
                    return ListTile(
                      title: Text(
                        '${entry.key}: ${entry.value}',
                        style: const TextStyle(fontSize: 20, color: Colors.black54),
                      ),
                    );
                  }).toList(),
                ],
              );
            } else if (snapshot.hasError) {
              return Center(
                child: Text(
                  'Error: ${snapshot.error}',
                  style: const TextStyle(fontSize: 20, color: Colors.black54),
                ),
              );
            }
          } else {
            return const Padding(
              padding: EdgeInsets.all(8.0),
              child: Column(
                children: [
                  MoneyBox(
                    title: "USD",
                    currency: "Bux",
                    amount: 1,
                    color: Colors.blueGrey,
                    size: 100,
                  )
                ],
              ),
            );
          }
          return const Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}
